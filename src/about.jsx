import React from 'react';
import ReactDOM from 'react-dom';

const hi = () => {
  //小括號包起來的就是用JSX建立的React element
  return (
    <div>
      Hello World!
    </div>
  );
};

export default hi